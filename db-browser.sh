#!/usr/bin/env bash

docker run --name=db-browser -d -p 3000:3000 --net="web-api" --link="api-mongo" mongoclient/mongoclient
