#!/bin/bash

docker stop db && docker rm db
docker run --name db -p 7001:27017 --volume "$(pwd)/mongodb/files/data:/data/db" -d mongo --auth

# docker exec -it db mongo admin -u root -p greenCross --authenticationDatabase admin
# docker exec -it queue /bin/bash
